package day10_2

import solveInputs

fun main() {
    solveInputs { input ->
        val adapters = input.lines().map { it.toInt() }.sorted()
        (adapters + (adapters.last() + 3)).fold(listOf(listOf(0))) { sublists, i ->
            if (i - sublists.last().last() == 3) sublists.plusElement(listOf(i))
            else sublists.dropLast(1).plusElement(sublists.last().plusElement(i))
        }.filter { it.size >= 3 }.map { sublist ->
            sublist.drop(1).dropLast(1).fold(listOf(sublist.take(1))) { choices, toggle ->
                choices + choices.map { it + toggle }
            }.map { it + sublist.last() }.filter { it.windowed(2).all { it[1] - it[0] <= 3 } }.size.toLong()
        }.reduce { acc, i -> acc * i }
    }
}
