package day17_2

import solveInputs
import utils.getAdjacent

fun main() {
    solveInputs { input ->
        val active = input.lines().flatMapIndexed { y, row ->
            row.chunked(1).mapIndexedNotNull { x, char ->
                if (char == "#") listOf(x, y, 0, 0) else null
            }
        }.toMutableSet()
        repeat(6) {
            active.toSet().let { previous ->
                (active + active.flatMap { it.getAdjacent() }).map { it to (it in active) }.forEach { (cube, isActive) ->
                    val count = cube.getAdjacent().count { it in previous }
                    if (isActive && count !in listOf(2, 3)) active -= cube
                    else if (!isActive && count == 3) active += cube
                }
            }
        }
        active.size
    }
}
