package day20_1

import solveInputs
import utils.Point

fun main() {
    solveInputs { input ->
        val tiles = input.split("\n\n").map { tileData ->
            val id = tileData.lines().first().substringAfter(" ").substringBefore(":").toInt()
            val tile = mutableMapOf<Point, Boolean>()
            tileData.lines().drop(1).forEachIndexed { y, line ->
                line.forEachIndexed { x, char ->
                    tile[Point(x, y)] = char == '#'
                }
            }
            id to tile
        }.toMap()
        val edges = tiles.map { (id, tile) ->
            id to getEdges(tile)
        }.toMap()
        val matches = tiles.keys.map {
            it to getMatchingTiles(it, edges)
        }.toMap()
        val corners = matches.filter { it.value.size == 2 }.map { it.key.toLong() }
        corners.reduce { acc, id -> acc * id }
    }
}

private fun getMatchingTiles(tile: Int, edges: Map<Int, List<List<Boolean>>>): List<Int> {
    return edges[tile]!!.mapNotNull { tileEdge ->
        val matching = edges.entries.filterNot { it.key == tile }.firstOrNull { (_, otherEdges) ->
            otherEdges.any { otherEdge -> otherEdge == tileEdge }
        }
        matching?.key
    }.distinct()
}

private fun getEdges(tile: Map<Point, Boolean>): List<List<Boolean>> {
    val maxY = tile.maxOf { it.key.y }
    val maxX = tile.maxOf { it.key.x }
    val top = (0..maxX).map { tile[Point(it, 0)]!! }
    val bottom = (0..maxX).map { tile[Point(it, maxY)]!! }
    val left = (0..maxY).map { tile[Point(0, it)]!! }
    val right = (0..maxY).map { tile[Point(maxX, it)]!! }
    return listOf(top, bottom, left, right, top.reversed(), bottom.reversed(), left.reversed(), right.reversed())
}

// Debugging

private fun printTile(tile: Map<Point, Boolean>) {
    val maxY = tile.maxOf { it.key.y }
    val maxX = tile.maxOf { it.key.x }
    for (y in 0..maxY) {
        for (x in 0..maxX) {
            val pixel = tile[Point(x, y)]!!
            print(if (pixel) "#" else ".")
        }
        println()
    }
}
