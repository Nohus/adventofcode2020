package day16_2

import solveInputs

data class Rule(
    val name: String,
    val ranges: List<IntRange>
)

fun main() {
    solveInputs { input ->
        val rules = input.substringBefore("\n\n").lines().map {
            val name = it.substringBefore(": ")
            val ranges = it.substringAfter(": ").split(" or ").map { range ->
                range.substringBefore("-").toInt()..range.substringAfter("-").toInt()
            }
            Rule(name, ranges)
        }
        val myTicket = input.substringAfter("your ticket:\n").substringBefore("\n\n").split(",").map { it.toLong() }
        val valid = input.substringAfter("nearby tickets:\n").lines().map {
            it.split(",").map { it.toInt() }
        }.map {
            it.filter { field -> rules.map { it.ranges }.flatten().any { rule -> field in rule } }
        }
        var possibleFields = rules.map { rule ->
            rule to (valid.first().indices).toList()
        }
        valid.forEach { ticket ->
            ticket.forEachIndexed { index, field ->
                possibleFields = possibleFields.map { possibilities ->
                    val isValid = possibilities.first.ranges.any { range -> field in range }
                    if (isValid) possibilities else possibilities.first to (possibilities.second - index)
                }
            }
        }
        val fieldMappings = mutableListOf<Pair<String, Int>>()
        while (possibleFields.isNotEmpty()) {
            possibleFields.filter { it.second.size == 1 }.forEach {
                fieldMappings += it.first.name to it.second.single()
            }
            possibleFields = possibleFields.filterNot { it.second.size == 1 }
            possibleFields = possibleFields.map {
                it.first to it.second.filter { it !in fieldMappings.map { it.second } }
            }
        }
        myTicket.filterIndexed { index, _ ->
            val name = fieldMappings.first { it.second == index }.first
            "departure" in name
        }.reduce { acc, i -> acc * i }
    }
}
