package day25_1

import solveInputs

fun main() {
    solveInputs { input ->
        val (cardPublicKey, doorPublicKey) = input.lines().map { it.toLong() }
        val cardLoopSize = transform(7).indexOf(cardPublicKey)
        transform(doorPublicKey).drop(cardLoopSize).first()
    }
}

private fun transform(subjectNumber: Long) = sequence {
    var value = 1L
    while (true) {
        value *= subjectNumber
        value %= 2020_12_27
        yield(value)
    }
}
