package day19_2

import day19_2.Rule.*
import solveInputs

sealed class Rule {
    data class IdSequence(val ids: List<String>) : Rule()
    data class IdOptions(val rules: List<IdSequence>) : Rule()
    open class TextType : Rule()
    data class TextSequence(val sequence: List<TextType>) : TextType() {
        override fun toString() = "(" + sequence.joinToString("") + ")"
    }
    data class TextOptions(val options: List<TextType>) : TextType() {
        override fun toString() = "(" + options.joinToString("|") + ")"
    }
    data class Text(val text: String) : TextType() {
        override fun toString() = text
    }
}

fun main() {
    solveInputs { input ->
        val (rulesInput, messagesInput) = input.split("\n\n")
        val rules = rulesInput.lines().map {
            val (id, ruleText) = it.split(": ")
            val rule = when {
                ruleText.contains("\"") -> Text(ruleText.removeSurrounding("\""))
                ruleText.contains("|") -> IdOptions(ruleText.split(" | ").map { IdSequence(it.split(" ")) })
                else -> IdOptions(listOf(IdSequence(ruleText.split(" "))))
            }
            id to rule
        }.toMap().toMutableMap()

        rules["8"] = IdOptions((1..5).map { IdSequence(List(it) { "42" }) })
        rules["11"] = IdOptions((1..5).map { IdSequence(List(it) { "42" } + List(it) { "31" }) })

        while (rules.values.any { it !is TextType }) {
            rules.toMap().forEach { (id, rule) ->
                if (rule is IdOptions) {
                    val simplified = rule.rules.map {
                        if (it.ids.all { rules[it] is TextType }) {
                            TextSequence(it.ids.map { rules[it]!! as TextType })
                        } else null
                    }
                    if (simplified.all { it != null }) {
                        rules[id] = TextOptions(simplified.filterNotNull())
                    }
                }
            }
        }
        val regex = rules["0"].toString().toRegex()
        messagesInput.lines().count { message -> message.matches(regex) }
    }
}
