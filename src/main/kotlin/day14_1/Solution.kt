package day14_1

import solveInputs

fun main() {
    solveInputs { input ->
        var mask = ""
        val memory = mutableMapOf<Int, String>()
        input.lines().forEach { line ->
            if (line.startsWith("mask")) {
                mask = line.substringAfter("= ")
            } else {
                val address = line.substringAfter("[").substringBefore("]").toInt()
                val value = line.substringAfter("= ").toInt().toString(2)
                memory[address] = (0 until 36).map { i ->
                    val maskDigit = mask[i]
                    val valueDigit = if (i < 36 - value.length) "0" else value[i - (36 - value.length)]
                    if (maskDigit == 'X') valueDigit else maskDigit
                }.joinToString("")
            }
        }
        memory.values.map { it.toLong(2) }.sum()
    }
}
