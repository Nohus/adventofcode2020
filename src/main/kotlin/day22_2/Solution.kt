package day22_2

import solveInputs

data class Result(val winner: Int, val deck: List<Int>)
data class HistoryEntry(val deckA: List<Int>, val deckB: List<Int>)

fun main() {
    solveInputs { input ->
        val (deckA, deckB) = input.split("\n\n").map { playerInput ->
            playerInput.lines().drop(1).map { it.toInt() }.toMutableList()
        }
        val result = combat(deckA.toMutableList(), deckB.toMutableList())
        result.deck.reversed().mapIndexed { index, i -> i * (index + 1) }.sum()
    }
}

private fun combat(deckA: MutableList<Int>, deckB: MutableList<Int>): Result {
    val history = mutableSetOf<HistoryEntry>()
    while (deckA.isNotEmpty() && deckB.isNotEmpty()) {
        val historyEntry = HistoryEntry(deckA.toList(), deckB.toList())
        if (historyEntry in history) { return Result(0, emptyList()) }
        history += historyEntry

        val a = deckA.removeFirst()
        val b = deckB.removeFirst()
        val winner = if (deckA.size >= a && deckB.size >= b) combat(deckA.take(a).toMutableList(), deckB.take(b).toMutableList()).winner
        else if (a > b) 0 else 1
        if (winner == 0) {
            deckA += a
            deckA += b
        } else {
            deckB += b
            deckB += a
        }
    }
    return if (deckA.isNotEmpty()) Result(0, deckA)
    else Result(1, deckB)
}
