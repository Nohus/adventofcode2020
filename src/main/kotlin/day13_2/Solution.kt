package day13_2

import solveInputs

fun main() {
    solveInputs { input ->
        input.lines()[1].split(",").asSequence()
            .mapIndexed { index, id -> index to id }
            .filter { it.second != "x" }
            .map { (index, id) -> "(x + $index) mod $id = 0" }.joinToString(", ")
        // Pasted into Wolfram Alpha to solve resulting equation for x
    }
}
