package day14_2

import solveInputs
import kotlin.math.pow

fun main() {
    solveInputs { input ->
        var mask = ""
        val memory = mutableMapOf<String, Long>()
        input.lines().forEach { line ->
            if (line.startsWith("mask")) {
                mask = line.substringAfter("= ")
            } else {
                val address = line.substringAfter("[").substringBefore("]").toInt().toString(2)
                val value = line.substringAfter("= ").toLong()
                val masked = (0 until 36).map { i ->
                    val addressDigit = if (i < 36 - address.length) "0" else address[i - (36 - address.length)]
                    if (mask[i] == '0') addressDigit else mask[i]
                }.joinToString("")
                if (masked.contains('X')) {
                    val floating = masked.count { it == 'X' }
                    for (floatingValue in 0 until (2.0.pow(floating).toInt())) {
                        var digits = floatingValue.toString(2).padStart(floating, '0')
                        val concreteAddress = masked.mapIndexed { index, char ->
                            if (char == 'X') digits.first().also { digits = digits.substring(1) }
                            else char
                        }.joinToString("")
                        memory[concreteAddress] = value
                    }
                } else memory[address] = value
            }
        }
        memory.values.sum()
    }
}
