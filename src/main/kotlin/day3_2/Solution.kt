package day3_2

import solveInputs
import utils.Point

fun main() {
    solveInputs { input ->
        val pattern = input.lines()

        val slopes = listOf(Point(1,1), Point(3, 1), Point(5, 1), Point(7, 1), Point(1, 2))
        slopes.map { slope ->
            var position = Point(0, 0)
            var trees = 0L
            while (position.y < pattern.size) {
                if (isTree(pattern, position)) trees++
                position += slope
            }
            trees
        }.reduce { acc, i -> acc * i }
    }
}

fun isTree(pattern: List<String>, pos: Point): Boolean {
    val row = pattern[pos.y]
    return row[pos.x % row.length] == '#'
}
