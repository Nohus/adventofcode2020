package day13_1

import solveInputs

fun main() {
    solveInputs { input ->
        val start = input.lines()[0].toInt()
        input.lines()[1].split(",")
            .filter { it != "x" }.map { it.toInt() }
            .map { it to (((start / it) * it) + it) }
            .minByOrNull { it.second }
            ?.let { (id, time) -> id * (time - start) }
    }
}
