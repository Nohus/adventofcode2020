package day15_1

import solveInputs

fun main() {
    solveInputs { input ->
        val history = mutableMapOf<Int, List<Int>>()
        var lastNumber = 0
        input.split(",").map { it.toInt() }.forEachIndexed { index, number ->
            history[number] = history.getOrDefault(number, emptyList()) + (index + 1)
            lastNumber = number
        }
        for (turn in (history.values.flatten().size + 1)..2020) {
            val number = if (history.getOrDefault(lastNumber, emptyList()).size > 1) {
                val (a, b) = history[lastNumber]!!.takeLast(2)
                b - a
            } else 0
            history[number] = (history.getOrDefault(number, emptyList()) + turn).takeLast(2)
            lastNumber = number
        }
        lastNumber
    }
}
