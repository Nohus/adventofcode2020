package day3_1

import solveInputs
import utils.Point

fun main() {
    solveInputs { input ->
        val pattern = input.lines()

        val slope = Point(3, 1)
        var position = Point(0, 0)
        var trees = 0
        while (position.y < pattern.size) {
            if (isTree(pattern, position)) trees++
            position += slope
        }
        trees
    }
}

fun isTree(pattern: List<String>, pos: Point): Boolean {
    val row = pattern[pos.y]
    return row[pos.x % row.length] == '#'
}
