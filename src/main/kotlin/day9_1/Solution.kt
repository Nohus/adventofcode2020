package day9_1

import solveInputs

fun main() {
    solveInputs { input ->
        input.lines().map { it.toLong() }.windowed(25 + 1).first { window ->
            val tail = window.dropLast(1)
            tail.none { window.last() - it in tail }
        }.last()
    }
}
