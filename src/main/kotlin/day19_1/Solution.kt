package day19_1

import day19_1.Rule.*
import solveInputs

sealed class Rule {
    data class IdSequence(val ids: List<String>) : Rule()
    data class IdOptions(val rules: List<IdSequence>) : Rule()
    open class TextType : Rule()
    data class TextSequence(val sequence: List<TextType>) : TextType() {
        override fun toString() = "(" + sequence.joinToString("") + ")"
    }
    data class TextOptions(val options: List<TextType>) : TextType() {
        override fun toString() = "(" + options.joinToString("|") + ")"
    }
    data class Text(val text: String) : TextType() {
        override fun toString() = text
    }
}

fun main() {
    solveInputs { input ->
        val (rulesInput, messagesInput) = input.split("\n\n")
        val rules = rulesInput.lines().map {
            val (id, ruleText) = it.split(": ")
            val rule = when {
                ruleText.contains("\"") -> Text(ruleText.removeSurrounding("\""))
                ruleText.contains("|") -> IdOptions(ruleText.split(" | ").map { IdSequence(it.split(" ")) })
                else -> IdOptions(listOf(IdSequence(ruleText.split(" "))))
            }
            id to rule
        }.toMap().toMutableMap()

        while (rules.values.any { it !is TextType }) {
            rules.toMap().forEach { (id, rule) ->
                if (rule is IdOptions) {
                    val simplified = rule.rules.map {
                        if (it.ids.all { rules[it] is TextType }) {
                            TextSequence(it.ids.map { rules[it]!! as TextType })
                        } else null
                    }
                    if (simplified.all { it != null }) {
                        rules[id] = TextOptions(simplified.filterNotNull())
                    }
                }
            }
        }

        val regexes = rules.values.map { it.toString().toRegex() }
        messagesInput.lines().count { message -> regexes.any { message.matches(it) } }
    }
}
