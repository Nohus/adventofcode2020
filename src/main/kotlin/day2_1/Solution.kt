package day2_1

import solveInputs

fun main() {
    solveInputs { input ->
        input.lines().count { line ->
            val (min, max, char, password) = line.split(": ", "-", " ")
            password.count { it == char[0] } in min.toInt()..max.toInt()
        }
    }
}
