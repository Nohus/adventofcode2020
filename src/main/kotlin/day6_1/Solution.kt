package day6_1

import solveInputs

fun main() {
    solveInputs { input ->
        input.split("\n\n").map { it.lines().flatMap { it.toCharArray().toList() }.toSet() }.map { it.size }.sum()
    }
}
