package day2_2

import solveInputs

fun main() {
    solveInputs { input ->
        input.lines().count { line ->
            val (a, b, char, password) = line.split(": ", "-", " ")
            (password.getOrNull(a.toInt() - 1) == char[0]) xor (password.getOrNull(b.toInt() - 1) == char[0])
        }
    }
}
