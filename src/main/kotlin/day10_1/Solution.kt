package day10_1

import solveInputs

fun main() {
    solveInputs { input ->
        val adapters = input.lines().map { it.toInt() }.sorted()
        val differences = (listOf(0) + adapters + (adapters.last() + 3)).windowed(2).map { it[1] - it[0] }
        differences.count { it == 1 } * differences.count { it == 3 }
    }
}
