package day21_2

import solveInputs

data class Food(val ingredients: List<String>, val allergens: List<String>)

fun main() {
    solveInputs { input ->
        val allergens = mutableMapOf<String, List<String>>()
        input.lines().map {
            Food(it.substringBefore(" (").split(" "),
                it.substringAfter("contains ").substringBefore(")").split(", "))
        }.let { foods ->
            foods.flatMap { it.allergens }.distinct().forEach { allergen ->
                foods.filter { allergen in it.allergens }.map { it.ingredients }.let { ingredients ->
                    allergens[allergen] = ingredients.first().filter { ingredient -> ingredients.all { ingredient in it } }
                }
            }
        }
        while (allergens.values.any { it.size > 1 }) {
            val knownIngredients = allergens.values.filter { it.size == 1 }.map { it.first() }
            allergens.toMap().filter { it.value.size > 1 }.forEach { (allergen, ingredients) ->
                allergens[allergen] = ingredients - knownIngredients
            }
        }
        allergens.toList().sortedBy { it.first }.joinToString(",") { it.second.first() }
    }
}
