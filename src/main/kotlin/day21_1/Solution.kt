package day21_1

import solveInputs

data class Food(val ingredients: List<String>, val allergens: List<String>)

fun main() {
    solveInputs { input ->
        input.lines().map {
            Food(it.substringBefore(" (").split(" "),
                it.substringAfter("contains ").substringBefore(")").split(", "))
        }.let { foods ->
            val possibleAllergenIngredients = foods.flatMap { it.allergens }.distinct().flatMap { allergen ->
                foods.filter { allergen in it.allergens }.map { it.ingredients }.let { ingredients ->
                    ingredients.first().filter { ingredient -> ingredients.all { ingredient in it } }
                }
            }.toSet()
            val noAllergenIngredients = foods.flatMap { it.ingredients }.distinct() - possibleAllergenIngredients
            noAllergenIngredients.map { ingredient -> foods.count { ingredient in it.ingredients } }.sum()
        }
    }
}
