package day18_2

import day18_2.Expression.Addition
import day18_2.Expression.Multiplication
import day18_2.Expression.Num
import day18_2.Expression.Parentheses
import solveInputs

sealed class Expression {
    abstract fun solve(): Long
    data class Num(val value: Long) : Expression() {
        override fun solve() = value
    }
    data class Parentheses(val value: Expression) : Expression() {
        override fun solve() = value.solve()
    }
    data class Addition(val a: Expression, val b: Expression) : Expression() {
        override fun solve() = a.solve() + b.solve()
    }
    data class Multiplication(val a: Expression, val b: Expression) : Expression() {
        override fun solve() = a.solve() * b.solve()
    }
}

private fun parse(text: String): Expression {
    text.trim().toLongOrNull()?.let { return Num(it) }
    return if (text.endsWith(")")) {
        var nesting = 0
        var startIndex = 0
        for (i in text.lastIndex downTo 0) {
            when (text[i]) {
                '(' -> nesting--
                ')' -> nesting++
            }
            if (nesting == 0) {
                startIndex = i
                break
            }
        }
        val right = Parentheses(parse(text.substring(startIndex).removeSurrounding("(", ")")))
        val remaining = text.substring(0, startIndex)
        if (remaining.isNotEmpty()) parseRemaining(remaining, right) else right
    } else {
        val right = Num(text.substringAfterLast(" ").toLong())
        val remaining = text.substringBeforeLast(" ")
        parseRemaining(remaining, right)
    }
}

private fun parseRemaining(remaining: String, right: Expression): Expression {
    val operation = remaining.trim().substringAfterLast(" ")
    val left = parse(remaining.trim().substringBeforeLast(" "))
    return when (operation) {
        "+" -> Addition(left, right)
        "*" -> Multiplication(left, right)
        else -> throw IllegalArgumentException()
    }
}

private fun updatePrecedence(expression: Expression): Expression {
    return when (expression) {
        is Num -> expression
        is Parentheses -> Parentheses(updatePrecedence(expression.value))
        is Multiplication -> Multiplication(updatePrecedence(expression.a), updatePrecedence(expression.b))
        is Addition -> when (expression.a) {
            is Multiplication -> Multiplication(updatePrecedence(expression.a.a), Addition(updatePrecedence(expression.a.b), updatePrecedence(expression.b)))
            else -> Addition(updatePrecedence(expression.a), updatePrecedence(expression.b))
        }
    }
}

fun main() {
    solveInputs { input ->
        input.lines().map {
            var exp = parse(it)
            while (true) {
                val prev = exp
                exp = updatePrecedence(exp)
                if (prev == exp) break
            }
            exp.solve()
        }.sum()
    }
}
