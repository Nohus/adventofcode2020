package day12_1

import solveInputs
import utils.Direction.*
import utils.Point
import utils.Turn.*

fun main() {
    solveInputs { input ->
        var ship = Point.ORIGIN
        var direction = EAST
        input.lines().forEach { line ->
            val value = line.substring(1).toInt()
            when (line.first()) {
                'N' -> ship = ship.move(NORTH, value)
                'S' -> ship = ship.move(SOUTH, value)
                'W' -> ship = ship.move(WEST, value)
                'E' -> ship = ship.move(EAST, value)
                'L' -> direction = direction.rotate(LEFT, value / 90)
                'R' -> direction = direction.rotate(RIGHT, value / 90)
                'F' -> ship = ship.move(direction, value)
            }
        }
        Point.ORIGIN.distanceTo(ship)
    }
}
