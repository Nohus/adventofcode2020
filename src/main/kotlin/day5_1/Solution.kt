package day5_1

import solveInputs
import kotlin.math.pow

fun main() {
    solveInputs { input ->
        input.lines().maxOf { pass ->
            val row = binaryPartition(pass.take(7), 'F')
            val column = binaryPartition(pass.drop(7), 'L')
            row * 8 + column
        }
    }
}

fun binaryPartition(code: String, frontCharacter: Char): Int {
    return code.fold(0 until 2.0.pow(code.length).toInt()) { range, char ->
        val half = (range.last + range.first) / 2
        if (char == frontCharacter) range.first..half
        else (half + 1)..range.last
    }.first
}
