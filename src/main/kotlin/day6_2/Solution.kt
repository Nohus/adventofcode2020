package day6_2

import solveInputs

fun main() {
    solveInputs { input ->
        input.split("\n\n").map { group ->
            val lines = group.lines()
            val answers = lines.flatMap { it.toCharArray().toList() }
            answers.toSet().count { char -> answers.count { char == it } == lines.size }
        }.sum()
    }
}
