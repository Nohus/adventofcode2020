package day9_2

import solveInputs

fun main() {
    solveInputs { input ->
        input.lines().map { it.toLong() }.let { numbers ->
            numbers.windowed(25 + 1).first { window ->
                window.dropLast(1).let { tail -> tail.none { window.last() - it in tail } }
            }.last().let { invalid ->
                numbers.indices.asSequence().mapNotNull { i ->
                    numbers.drop(i).fold(emptyList<Long>()) { acc, l -> if (acc.sum() < invalid) acc + l else acc }.let { elements ->
                        if (elements.sum() == invalid) elements.minOrNull()!! + elements.maxOrNull()!! else null
                    }
                }.first()
            }
        }
    }
}
