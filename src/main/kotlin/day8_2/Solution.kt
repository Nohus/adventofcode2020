package day8_2

import solveInputs

data class Instruction(
    val name: String,
    val argument: Int
)

fun main() {
    solveInputs { input ->
        val code = input.lines().map {
            val (name, argument) = it.split(" ")
            Instruction(name, argument.toInt())
        }
        code.asSequence().mapIndexedNotNull { i, instruction ->
            val new = when (instruction.name) {
                "nop" -> instruction.copy(name = "jmp")
                "jmp" -> instruction.copy(name = "nop")
                else -> return@mapIndexedNotNull null
            }
            code.toMutableList().apply { set(i, new) }
        }.mapNotNull(::run).first()
    }
}

fun run(code: List<Instruction>): Int? {
    var accumulator = 0
    var pc = 0
    val visited = mutableListOf<Int>()
    while (true) {
        val instruction = code[pc]
        visited += pc
        when (instruction.name) {
            "acc" -> accumulator += instruction.argument
            "jmp" -> pc += instruction.argument - 1
        }
        pc++
        if (pc in visited) return null
        if (pc > code.lastIndex) return accumulator
    }
}
