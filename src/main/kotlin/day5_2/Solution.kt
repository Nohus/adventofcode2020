package day5_2

import solveInputs
import kotlin.math.pow

fun main() {
    solveInputs { input ->
        val seats = input.lines().map { pass ->
            val row = binaryPartition(pass.take(7), 'F')
            val column = binaryPartition(pass.drop(7), 'L')
            row * 8 + column
        }
        val min = seats.minOrNull()!! + 1
        val max = seats.maxOrNull()!! - 1
        (min..max).toList().first { it !in seats }
    }
}

fun binaryPartition(code: String, frontCharacter: Char): Int {
    return code.fold(0 until 2.0.pow(code.length).toInt()) { range, char ->
        val half = (range.last + range.first) / 2
        if (char == frontCharacter) range.first..half
        else (half + 1)..range.last
    }.first
}
