package day11_2

import day11_2.Tile.*
import solveInputs
import utils.Point

enum class Tile {
    FLOOR, EMPTY, OCCUPIED
}

fun main() {
    solveInputs { input ->
        val inputMap = mutableMapOf<Point, Tile>()
        input.lines().forEachIndexed { y, line ->
            line.forEachIndexed { x, char ->
                inputMap[Point(x, y)] = if (char == 'L') EMPTY else FLOOR
            }
        }
        var map: Map<Point, Tile> = inputMap
        while (true) {
            val newMap = applyRound(map)
            if (newMap == map) break
            map = newMap
        }
        map.count { (_, tile) -> tile == OCCUPIED }
    }
}

private fun applyRound(map: Map<Point, Tile>): Map<Point, Tile> {
    val new = mutableMapOf<Point, Tile>()
    map.forEach { (point, tile) ->
        when {
            tile == EMPTY && getAdjacent(map, point).none { it == OCCUPIED } -> new[point] = OCCUPIED
            tile == OCCUPIED && getAdjacent(map, point).count { it == OCCUPIED } >= 5 -> new[point] = EMPTY
            else -> new[point] = map[point]!!
        }
    }
    return new
}

private fun getAdjacent(map: Map<Point, Tile>, point: Point): List<Tile> {
    return listOf(
        Point(-1, -1), Point(0, -1), Point(1, -1),
        Point(-1, 0), Point(1, 0),
        Point(-1, 1), Point(0, 1), Point(1, 1),
    ).map { getSeat(map, point, it) }
}

private fun getSeat(map: Map<Point, Tile>, point: Point, direction: Point): Tile {
    var current = Point(point.x + direction.x, point.y + direction.y)
    while (true) {
        val tile = map[current] ?: return FLOOR
        if (tile in listOf(EMPTY, OCCUPIED)) return tile
        current = Point(current.x + direction.x, current.y + direction.y)
    }
}
